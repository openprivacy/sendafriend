## sendafriend

WARNING: sendafriend has not been audited and probably contains security vulnerabilities. please do not use it for anything sensitive yet

quickly send files or control signals between machines by piping them on the command line, with bidirectional authentication and metadata resistance via tor (built on cwtch)

includes a way to quickly exchange onion addresses using a low-entropy shared secret (using pairwise socialist millionaires' protocol on a bbs) and a contact manager

note that we only support data up to 64k right now, sorry :(  (a fix for this is in the works)

sendafriend is rated Adults Only because it generates random words sometimes and i haven't read all the words personally

## installation

requires you to have go installed and your gopath configured correctly (sorry). it also requires tor to be installed and running, with no authentication on the default control port (sorry, fixing now)

`go get openprivacy.ca/openprivacy/sendafriend`

## usage

one-time setup, on both machines:
`sendafriend pair [make-up-a-name]`

activate the receiver first:
`sendafriend receive [made-up-name] > file.ext`

then the sender:
`sendafriend send [made-up-name] < file.ext`

the pairing feature is experimental and kinda flakey right now. you may find it easier to `sendafriend info` to get your identity string, transfer it over some other secure channel, and then `sendafriend add [make-up-a-name] [identity string]` to add it to the contact list

## manage multiple identities

to have multiple addresses and contact lists, create a directory to hold them and prefix commands like so:

`env SENDAFRIEND_FOLDER=$HOME/bob sendafriend info`

## <3

made with love by errorinn